﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBullet : MonoBehaviour {
    // separate speed and direction so we can 
    // tune the speed without changing the code

    public float speed = 10.0f;
    public Vector3 direction;
    private Rigidbody rigidbody;
	float delay = 2.0f;

	// Use this for initialization
	void Start () {
        rigidbody = GetComponent<Rigidbody>();
	}

    private void FixedUpdate()
    {
        rigidbody.velocity = speed * direction;
    }

    // Update is called once per frame
	void Update () {
		Destroy(gameObject, delay);
	}

    private void OnCollisionEnter(Collision collision)
    {
        // Destroy the bullet
        Destroy(gameObject);
    } 
}
