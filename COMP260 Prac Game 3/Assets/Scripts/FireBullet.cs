﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(AudioSource))]
public class FireBullet : MonoBehaviour {
    public MoveBullet bulletPrefab;
    public AudioClip shootSound;
    private AudioSource audio;
	private bool readyToFire = true;
	
	// Use this for initialization
	void Start () {
        audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        if(Time.timeScale == 0)
        {
            return;
        }

		if (readyToFire) {
		// when the button is pushed, fire a bullet
			if (Input.GetButtonDown("Fire1"))
			{
				MoveBullet bullet = Instantiate(bulletPrefab);
				// the bullet starts at the player's position 
				bullet.transform.position = transform.position;

				// create a ray towards the mouse location 
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				bullet.direction = ray.direction;
                audio.PlayOneShot(shootSound);

				readyToFire = false;
				
				StartCoroutine(resetFire());
			}
		}
	}
	
	IEnumerator resetFire() {
		yield return new WaitForSeconds(1);
		readyToFire = true;
	}
}
