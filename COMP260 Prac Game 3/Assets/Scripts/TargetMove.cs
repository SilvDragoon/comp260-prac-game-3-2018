﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetMove : MonoBehaviour
{
    public float startTime = 0.0f;

    private Animator animator;

    // Use this for initialization
    void Start()
    {
        // get the animator component
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        // set the Start parameter to true
        // if we have passed the start time
        if (Time.time >= startTime)
        {
            animator.SetTrigger("Start");
        }
    }

    void Destroy()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        // transition to the Die animation
        animator.SetTrigger("Hit");
        Debug.Log("Hit targer");
    }
}
